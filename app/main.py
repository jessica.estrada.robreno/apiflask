from flask import Flask, jsonify

app = Flask(__name__)
@app.route('/ping', methods=['GET'])
def ping():
    return jsonify({'response': 'pong!'})

@app.route('/message')
def getMessage():
    return jsonify({'mgs': 'HOLA MUNDO'})

if __name__ == '__main__':
    app.run(debug=True, port=80)